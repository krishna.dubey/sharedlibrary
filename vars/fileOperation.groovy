def copyFile(Map parameter) {
    def fileSourcePath  = "${parameter.fileSourcePath}"
    def fileDestinationPath     = "${parameter.fileDestinationPath}"
    stage("${parameter.stageName}") {
        try {
            sh "cp -rf ${fileSourcePath} ${fileDestinationPath}"
        }
        catch (Exception e) {
            notification.slackNotifyFailure ( "job fail at this stage ${parameter.stageName}")
            println(e.getMessage());
            throw e
        }
    }
}
        
