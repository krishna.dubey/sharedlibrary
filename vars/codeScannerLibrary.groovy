def snykCodeScan(Map parameter) {
    stage("${parameter.stageName}") {
        try {
            snykSecurity failOnIssues: false, 
            snykInstallation: "${parameter.snykInstallation}", 
            snykTokenId: "${parameter.snykTokenId}"

        }
        catch (Exception e) {
            notification.slackNotifyFailure ( "job fail at this stage ${parameter.stageName}")
            println(e.getMessage());
            throw e
        }
    }
}
