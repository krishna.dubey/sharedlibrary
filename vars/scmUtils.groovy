def checkoutcode(Map parameter) {
    def GIT_URL = "${parameter.GIT_URL}"
    def BRANCH  = "${parameter.BRANCH}"
    stage("${parameter.stageName}") {
        try {
            checkout([
                $class: 'GitSCM',
                branches: [[name:  "${BRANCH}" ]],
                userRemoteConfigs: [[ url: "${GIT_URL}" ]]
           ])
        }
        catch (Exception e) {
            notification.slackNotifyFailure ( "job fail for cloning the code")
            println(e.getMessage());
            throw e
        }
    }
}   
    def checkoutScm(){
        checkout scm
    }