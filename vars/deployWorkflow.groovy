#!/usr/bin/env groovy
def call(Map parameter) {
    scmUtils.checkoutScm()
    config = pipelineConfig.readPropFiles(
        configFile: "${parameter.configFile}"
    )
    scmUtils.checkoutcode(
        stageName: "checkout SCM",
        GIT_URL:   "${config.GIT_URL}",
        BRANCH:    "${config.BRANCH}"
    )
    deployServer.deployArtifact(
        stageName:           "code deploy to server",
        deployScriptPath:    "${config.DEPLOY_SCRIPT_PATH}",
        serverIp:            "${config.SERVER_IP}",
        fileDestinationPath: "${config.FILE_DESTINATION_PATH}"
    )
    notification.slackNotifySuccess( "Nodejs Code Deploy SuccessFul")
}
