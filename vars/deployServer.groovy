def deployArtifact(Map parameter) {
    def deployScriptPath        = "${parameter.deployScriptPath}"
    def serverIp                = "${parameter.serverIp}"
    def fileDestinationPath     = "${parameter.fileDestinationPath}"
    stage("${parameter.stageName}") {
        try {
            sh "bash ${deployScriptPath} '${serverIp}' ${fileDestinationPath}"
        }
        catch (Exception e) {
            notification.slackNotifyFailure ( "job fail at this stage ${parameter.stageName}")
            println(e.getMessage());
            throw e
        }
    }
}