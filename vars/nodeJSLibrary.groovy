def nodeJsOpreations(Map parameter) {
   stage("${parameter.stageName}") {
      try {  
         sh "npm ${parameter.opreations}"
      }
      catch(Exception e) {
         notification.slackNotifyFailure( "job fail at this stage ${parameter.stageName}")
         println(e.getMessage());
	      throw e
      }
   }
} 
