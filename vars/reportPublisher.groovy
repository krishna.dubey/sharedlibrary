def htmlReport(Map parameter) {
    def htmlReportDir = "${parameter.htmlReportDir}"
    def htmlReportName = "${parameter.htmlReportName}"
    stage("${parameter.stageName}") {
        try {  
            publishHTML([
              allowMissing: false, 
              alwaysLinkToLastBuild: false, 
              keepAll: false, 
              reportDir: "${htmlReportDir}", 
              reportFiles: 'index.html', 
              reportName: "${htmlReportName}", 
              reportTitles: ''])
        }
        catch (Exception e) {
            notification.slackNotifyFailure ( "job fail at this stage ${parameter.stageName}")
            println(e.getMessage());
            throw e
        }
}
}