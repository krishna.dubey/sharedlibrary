def slackNotifyBuild(message, colorCode) {
    wrap([$class: 'BuildUser']) {
    def user = env.BUILD_CAUSE
    }
    def subject = "Message : ${message}  \n Job Name : '${env.JOB_NAME}' \n Job Build Number: '[${env.BUILD_NUMBER}]' "
    def summary = "${subject} \n  Job Url: (${env.BUILD_URL}) \n BUILD_USER : ${user} "
    slackSend (color: colorCode , message: summary)
}
 def slackNotifySuccess(message) {
    slackNotifyBuild(message, 'good')
 }

 def slackNotifyFailure(message) {
    slackNotifyBuild(message, 'danger')  
 }  