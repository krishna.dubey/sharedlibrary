#!/usr/bin/env groovy
def call(Map parameter) {
    scmUtils.checkoutScm()
    config = pipelineConfig.readPropFiles(
        configFile: "${parameter.configFile}"
    )
    scmUtils.checkoutcode(
        stageName: "checkout SCM",
        GIT_URL:   "${config.GIT_URL}",
        BRANCH:    "${config.BRANCH}"
    )
    nodeJSLibrary.nodeJsOpreations(
        stageName:  "Install NodeJS Dependency",
        opreations: 'install'
    )
    if ("${config.CHECK_LINTING}" != "false") {
        nodeJSLibrary.nodeJsOpreations(
            stageName:  'NodeJS Code Linting',
            opreations: 'run lint'
        )
    }
    if ("${config.CODE_COMPILE}" != "false") {
        nodeJSLibrary.nodeJsOpreations(
            stageName:  "NodeJS Code Compile",
            opreations: 'run compile'
        )
    }
    parallel 'Unit Test': {
        nodeJSLibrary.nodeJsOpreations(
            stageName:  "Test nodejs code",
            opreations: 'run test'
        )
    },
    'code coverage': {
        nodeJSLibrary.nodeJsOpreations(
            stageName:  "code coverage",
            opreations: 'run test:coverage -- --runInBand'
        )
    }
    if ("${config.SNYK_SCAN}" != "false") {
        codeScannerLibrary.snykCodeScan(
            stageName:        "scan code Vulnerabilities with Snyk ",
            snykInstallation: "${config.SNYK_INSTALLATION}",
            snykTokenId:      "${config.SNYK_TOKEN_ID}"
        )
    }
    reportPublisher.htmlReport(
        stageName:      "publish nodeJS Report",
        htmlReportDir:  "${config.HTML_REPORT_DIR}",
        htmlReportName: "${config.HTML_REPORT_NAME}"
    )
    fileOperation.copyFile(
        stageName: "set env config",
        fileSourcePath: "${config.ENV_CONFIG_FILE_PATH}",
        fileDestinationPath: "${config.FILE_DESTINATION_PATH}"
    )
    nodeJSLibrary.nodeJsOpreations(
        stageName:  "build artifact",
        opreations: "run build:${config.CODE_NAME}"
    )
    publishArtifactS3.publishArtifactS3(
        stageName:        "publish artifact to S3",
        artifactName:     "${config.ARTIFACT_NAME}",
        bucketName:       "${config.BUCKET_NAME}"
    )
    deployServer.deployArtifact(
        stageName:        "code deploy to server",
        deployScriptPath: "${config.DEPLOY_SCRIPT_PATH}",
        serverIp:         "${config.SERVER_IP}",
        fileDestinationPath: "${config.FILE_DESTINATION_PATH}"
    )
    notification.slackNotifySuccess( "Nodejs stg-react Pipeline Run SuccessFul")

}
