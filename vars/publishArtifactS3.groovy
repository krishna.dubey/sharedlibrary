def publishArtifactS3(Map parameter) {
    def artifactName      = "${parameter.artifactName}"
    def bucketName        = "${parameter.bucketName}"
    stage("${parameter.stageName}") {
        try {
            sh "aws s3 cp ${artifactName} s3://${bucketName}/"
        }
        catch (Exception e) {
            notification.slackNotifyFailure ( "job fail at this stage ${parameter.stageName}")
            println(e.getMessage());
            throw e
        }
    }
}